<cfinclude template="plugin/config.cfm" />
<cfparam name="form.authKey" default="">

<cfif isDefined("form.issubmitted")>
	<cfset gistObject		= createObject("component","mura.extend.extendObject").init(Type="Custom",SubType="Gist",SiteID=session.siteID)>
	<cfset gistObject.setID( session.siteID ) />
	<cfset gistObject.setValue('AuthKey',form.authKey ) />
	<cfset gistObject.save() />
<cfelse>
	<cfset gistObject	= createObject("component","mura.extend.extendObject").init(Type="Custom",SubType="Gist",SiteID=session.siteID)>
	<cfset gistObject.setID( session.siteID ) />
	<cfset form.authKey =  gistObject.getValue('Authkey') />
</cfif>
<cfsavecontent variable="variables.body">
	<cfoutput>
	<cfset qSiteAssigned = request.pluginConfig.getAssignedSites()>
	<h1>Gist Plugin</h1>
	<h2>Create Gist right from your blog post and render with Gist syntax highlighting tool.</h2>
	<script language="JavaScript">
		function submitForm(){
			document.getElementById('frmsettings').submit();
		}
	</script>
	<div class="tab-content row-fluid">
		<cfset authkey =  request.pluginConfig.getSetting('authkey')>
		<form name="frmsettings" id="frmsettings" method="post">
			<input type="hidden" name="issubmitted" value="1"/>
			<div class="control-group">
				<lable class="control-label">
					<a href="##" rel="tooltip" data-original-title="Generate authorization key at https://github.com/settings/applications">Auth Key: <i class="icon-question-sign"></i></a>
				</label>
				<div class="control">
					<input type="text" id="authkey" name="authkey" value="#form.authKey#" maxlength="255" class="span12"/>
					<br/>
					<span>Authorization key required if you want to create gist.</span>
				</div>
			</div>
			<div class="form-actions">
				<button type="button" class="btn" onclick="submitForm()"><i class="icon-check"></i> Save Settings</button>
			</div>
		</form>
	</div>
	<div>
		<p>
			<h2>Generate Authorization Key</h2>
			<div>
				Visit URL <a href="https://github.com/settings/applications" target="_new">https://github.com/settings/applications</a>. Login to your github account if not already, and generate new "Personal Access Tokens" by clicking on "Create new token" button. It may ask for your token description, give some meaningfull name and "Create Token".
			</div>
		</p>
		<p><h2>Sample Gist Tags</h2></p>
		<table class="table table-bordered" >
			<thead>
			<tr>
				<th colspan="2">Sample Gist Tags</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td style="text-align:left!important">
					<b>[gist url="isummation/6218033" /]</b>
				</td>
				<td>
					Render code from given id to your blog post. <br/>
					'/' at the end required.
				</td>
			</tr>
			<tr>
				<td style="text-align:left!important">
					<b>[gist]</b><br/>
						Your code will goes here<br/>
					<b>[/gist]</b>

				</td>
				<td>
					Create new Gists for your code if authorisation code available. You can specify different attributes within your tag. 
				</td>
			</tr>
			<tr>
				<td style="text-align:left!important">
					<b>[gist url="yourid/gistid" filename="myblogcode.cfm" public="true"]</b><br/>
						Your coldfusion code will goes here<br/>
					<b>[/gist]</b>

				</td>
				<td>
					If you haven't specified URL (yourid/gistid) plugin will create new gist for code and automatically update URL in your blog post otherwise it will update it. 
				</td>
			</tr>
			</tbody>
		</table>
		<p>
			<h2>Supported Attributes </h2>
			<ul>
				<li><b>url</b> : [username]/[gistid]</li>
				<li><b>filename</b> : Filename attribute required if you want to create gist from your post. Use filename extension to suggest sysntex highlighting. (e.g. mycode.cfm if you wnat to use coldfusion syntex.</li>
				<li><b>public</b> : true/false (Default value is true, you can set it to 'false' if you want to create secret gist.</li>

			</ul>
		</p>
		</div>
	</cfoutput>
</cfsavecontent>
<cfoutput>#application.pluginManager.renderAdminTemplate(body=variables.body,pageTitle=request.pluginConfig.getName())#</cfoutput>