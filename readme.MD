Gist Plugin
===========

Introduction
------------

Gist plugin is simple yet powerful to manage your gist code right from your blog post. This will help you to display your/third party gists to your post just by adding [gist] tag and adding URL as attributes. Above of this, whenever you are adding code within your blog post will automatically create gist for you. Whenever you update code in your blog post will automatically update it to your gists as well. This can be done by just wrapping your code with [gist] tag and providing required attributes.


Generate Authorization Key
--------------------------

Visit URL https://github.com/settings/applications. 
Login to your github account if not already, and generate new "Personal Access Tokens" by clicking on "Create new token" button. It will ask for your token description, give some meaningfull name and "Create Token".


Sample Gist Tags
----------------

[gist url="isummation/6218033" /]
Render code from given id to your blog post. '/' at the end required.


[gist]
Your code will goes here
[/gist]	 

Create new Gists for your code if authorisation code available.


[gist url="yourgithubusername/gistid" filename="myblogcode.cfm" public="true"]
Your coldfusion code will goes here
[/gist]	 

* If you haven't specified URL (yourid/gistid) plugin will create new gist for code and automatically update URL in your blog post otherwise it will update it.
* Filename is required parameter and extention of filename will use to determine sysntex highlighting.


Supported Attributes
--------------------

url : [username]/[gistid]
filename : Filename attribute required if you want to create gist from your post. Use filename extension to suggest sysntex highlighting. (e.g. mycode.cfm if you wnat to use coldfusion syntex.
public : true/false (Default value is true, you can set it to 'false' if you want to create secret gist.


Limitation
----------
Plugin will not work with code added in "Summary" field.


License
-------

The Gist Plugin is licensed under the GPL 2.0 license.
Copyright (C) 2013 Pritesh Patel http://www.thecfguy.com
