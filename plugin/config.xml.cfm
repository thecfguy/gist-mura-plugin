<?xml version="1.0" encoding="UTF-8"?>
<plugin>
	<name>Gist</name>
	<package>gist</package>
	<directoryFormat>packageOnly</directoryFormat>
	<loadPriority>5</loadPriority>
	<version>0.3</version>
	<provider>Pritesh Patel</provider>
	<providerURL>http://thecfguy.com/</providerURL>
	<category>Utility</category>
	<settings></settings>
	<EventHandlers>
		<eventHandler event="onBeforeContentSave" component="eventHandlers.GistHandler" persist="true" />
		<eventHandler event="onRenderEnd" component="eventHandlers.GistHandler" persist="true" />
	</EventHandlers>
	<DisplayObjects />
</plugin>
