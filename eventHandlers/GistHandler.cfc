<cfcomponent extends="mura.plugin.pluginGenericEventHandler">
	<cffunction name="onBeforeContentSave" access="public" returntype="void" output="false">
		<cfargument name="event" />
		<cfset var local = structNew()>
		<cfset local.siteid = arguments.event.getValue('siteid')>
		<cfset local.resContent = arguments.event.getValue('Body')>
		<cfset local.gistObject	= createObject("component","mura.extend.extendObject").init(Type="Custom",SubType="Gist",SiteID=local.siteid)>
		<cfset local.gistObject.setID(local.siteid) />
		<cfset local.authKey =  gistObject.getValue('Authkey') />
		<cfset local.gistAPI = createObject("component","#variables.pluginConfig.getDirectory()#.Gist").init(local.authKey)>
		<cfset local.gistTagRegex = "\[gist[^\]]*?(?:\/]|\]([\s\S]*?)\[\/gist\])">
		<cfset local.gistAttrRegex = "(\burl\b|\bpublic\b|\bfilename\b)\s*=\s*['""]([^'""]+)['""]">
		<cfset local.gistTags = rematch(local.gistTagRegex,local.resContent)>

		<!--- Loop through all gist tags --->
		<cfloop array="#local.gistTags#" index="local.gistTag">
			<cfset local.attributes=rematch(local.gistAttrRegex,htmldecode(local.gistTag))>
			<cfset local.args = structNew()>
			<cfset local.originalcontent=rereplacenocase(local.gistTag,local.gistTagRegex,"\1")>
			<cfset local.args.content = reReplaceNoCase(local.originalcontent, "<[\/]*[\S\s]+?[^>]*>",'',"All")>	<!--- Remove html tags --->
			<cfset local.args.content = htmldecode(local.args.content)>
			<cfset local.args.content = reReplaceNoCase(local.args.content,"(\n)[\t]+","\1","All")>
			<!--- Handle Attributes --->
			<cfloop array="#local.attributes#" index="local.attribute">
				<cfset local.attrname = rereplace(local.attribute,local.gistAttrRegex,"\1")>
				<cfset local.attrvalue = rereplace(local.attribute,local.gistAttrRegex,"\2")>
				<cfswitch expression="#local.attrname#">
					<cfcase value="url">
						<cfset local.args.id = listlast(local.attrvalue,"/")>
						<cfset local.args.userid = listfirst(local.attrvalue,"/")>
					</cfcase>
					<cfcase value="public">
						<cfset local.args.ispublic = listlast(local.attrvalue,"/")>
					</cfcase>
					<cfcase value="filename">
						<cfset local.args.filename = listlast(local.attrvalue,"/")>
					</cfcase>
				</cfswitch>
			</cfloop>
			<cftry>
			<cfif structKeyExists(local.args,"id")>
				<cfif len(trim(local.args.content))>	<!--- Do not update if there is not content inside --->
					<cfset local.currentGist = local.gistAPI.getGist(id=local.args.id)>
					<cfset local.args.newfilename = local.args.filename>
					<cfset local.args.filename = structkeyarray(local.currentGist.files)[1]>
					<cfset local.res =  local.gistAPI.updateGist(argumentcollection=local.args)>
					<cfset local.args.id = local.res.id>
					<cfset local.args.userid = local.res.owner.login>
					<cfset local.gistContent = htmlencode(createGISTTag(local.args)) & local.originalcontent & "[/gist]" >
					<cfset local.resContent = replacenocase(local.resContent,local.gistTag,local.gistContent)>
				</cfif>
			<cfelse>
				<cfset local.res =  local.gistAPI.createGist(argumentcollection=local.args)>
				<cfset local.args.ispublic = iif(local.res.public,de("true"),de("false"))>
				<cfset local.args.id = local.res.id>
				<cfset local.args.userid = local.res.owner.login>
				<cfset local.args.filename = structkeyarray(local.res.files)[1]>
				<cfset local.gistContent = htmlencode(createGISTTag(local.args)) & local.originalcontent & "[/gist]" >
				<cfset local.resContent = replacenocase(local.resContent,local.gistTag,local.gistContent)>
			</cfif>
			<cfcatch>
				<!--- Do nothing if error occurred at gist call --->
			</cfcatch>
			</cftry>
		</cfloop>

		<cfset local.contentBean = arguments.event.getValue('contentBean')>
		<cfset local.contentBean.setValue('body',local.resContent)>
		<cfset arguments.event.setValue('Body',local.resContent) />
	</cffunction>


	<cffunction name="onRenderEnd" access="public" returntype="void" output="false">
		<cfargument name="event" />
		<cfset var $ = application.serviceFactory.getBean('MuraScope').init() />
		<cfset var local = {} />
		<cfset local.resContent = arguments.event.getValue('__MuraResponse__') />
		<cfset local.gistTagRegex = "\[gist[^\]]*?(?:\/]|\]([\s\S]*?)\[\/gist\])">
		<cfset local.gistAttrRegex = "(\burl\b)\s*=\s*['""]([^'""]+)['""]">
		<cfset local.gistTags = rematch(local.gistTagRegex,local.resContent)>

		<cfset local.gisturl = "">
		<cfif arrayLen(local.gistTags)>
			<cfloop array="#local.gistTags#" index="local.code">
				<cfset local.attr = rematchnocase(local.gistAttrRegex,htmldecode(local.code))>
				<cfif arrayLen(local.attr)>
					<cfset local.gitURL = rereplacenocase(local.attr[1],local.gistAttrRegex,"\2")>
					<cfset local.gistURL = '<script src="https://gist.github.com/#local.gitURL#.js"></script>'>
					<cfset local.resContent = replacenocase(local.resContent,local.code,local.gisturl)>
				</cfif>
			</cfloop>
			<cfset arguments.event.setValue('__MuraResponse__',local.resContent) />
		</cfif>
	</cffunction>


	<cffunction name="createGISTTag" access="private" returntype="string">
		<cfargument name="params" type="struct" required="true">
		<cfset var gistTag = "[gist " >
		<cfset var attr= "">
		<cfloop array="#structKeyArray(arguments.params)#" index="attr">
			<cfswitch expression="#attr#">
			<cfcase value="id">
				<cfset gistTag = gistTag & " url=""#arguments.params['userid']#/#arguments.params['id']#""">
			</cfcase>
			<cfcase value="ispublic">
				<cfset gistTag = gistTag & " public=""#arguments.params[attr]#""">
			</cfcase>
			<cfcase value="filename">
				<cfset gistTag = gistTag & " filename=""#arguments.params[attr]#""">
			</cfcase>
			</cfswitch>
		</cfloop>
		<cfset gistTag = gistTag & "]">
		<cfreturn gistTag>
	</cffunction>

	<cffunction name="htmldecode" access="private" returntype="string">
		<cfargument name="encodedStr" type="string" required="true">
		<cfreturn replacelist(arguments.encodedStr,"&lt;,&gt;,&quot;,&nbsp;","<,>,"", ")>
	</cffunction>
	<cffunction name="htmlencode" access="private" returntype="string">
		<cfargument name="htmlstring" type="string" required="true">
		<cfset arguments.htmlstring = replacelist(arguments.htmlstring,"<,>,"", ","&lt;,&gt;,&quot;,&nbsp;")>
		<cfreturn arguments.htmlstring>
	</cffunction>
</cfcomponent>
