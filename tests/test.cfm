<cfsavecontent variable = "content">
	<cfoutput><p>I am setting up nginx in my development environment. I am using windows 10, Lucee with iis and nodejs with expressjs and iisnode. To replace IIS with nginx, first I&nbsp;downloaded nginx and extracted in <code>C:/nginx</code>. Before we configure nginx, first let's configure Lucee. I had&nbsp;installed Lucee installer and running on&nbsp;8888 port. Now add you website entry in engine tag as follow:</p>

	<p>[gist url=&quot;isummation/ac1ebcd320e3e972c13b0e1e9c309c94&quot; filename=&quot;nginx-conf-1.xml&quot;]</p>

	<p><br />
		&lt;!-- ADD NEW HOSTS HERE --&gt;<br />
		&lt;!-- VIRTUAL HOST --&gt;<br />
		&lt;Host name=&quot;mysite.local&quot; appBase=&quot;webapps&quot;&gt;<br />
		&lt;Context path=&quot;&quot; docBase=&quot;C:/mysite.local&quot; /&gt;<br />
		&lt;/Host&gt;</p>

	<p>[/gist]</p>

	<p>Now, create a <code>lucee.conf</code> file and add in <code>C:/nginx/conf</code> folder with following content.</p>

	<p>[gist url=&quot;isummation/901b6a7628b3a6263948637079ddf012&quot; filename=&quot;nginx-conf-2.xml&quot;]</p>

	<p>## Main Lucee proxy handler<br />
		location ~ \.(cfm|cfml|cfc|jsp|cfr)(.*)$ {<br />
		&nbsp;&nbsp; &nbsp;proxy_pass http://127.0.0.1:8888;<br />
		&nbsp;&nbsp; &nbsp;proxy_redirect off;<br />
		&nbsp;&nbsp; &nbsp;proxy_set_header Host $host;<br />
		&nbsp;&nbsp; &nbsp;proxy_set_header X-Forwarded-Host $host;<br />
		&nbsp;&nbsp; &nbsp;proxy_set_header X-Forwarded-Server $host;<br />
		&nbsp;&nbsp; &nbsp;proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;<br />
		&nbsp;&nbsp; &nbsp;proxy_set_header X-Forwarded-Proto $scheme;<br />
		&nbsp;&nbsp; &nbsp;proxy_set_header X-Real-IP $remote_addr;<br />
		}</p>

	<p>##This is for static content<br />
		location ~* \.(jpg|jpeg|png|gif|ico|css|js)$ {<br />
		&nbsp;&nbsp; &nbsp;access_log off;<br />
		&nbsp;&nbsp; &nbsp;gzip_static on;<br />
		&nbsp;&nbsp; &nbsp;expires 1w;<br />
		&nbsp;&nbsp; &nbsp;add_header Cache-Control public;<br />
		&nbsp;&nbsp; &nbsp;add_header ETag &quot;&quot;;<br />
		}<br />
		[/gist]</p>

	[gist url=&quot;isummation/6305596&quot; filename=&quot;test.cfm&quot; /]
</cfoutput>
</cfsavecontent>


<cfset onBeforeContentSave()>
<cfset onRenderEnd()>

<cffunction name="onBeforeContentSave" access="public" returntype="void" output="false">
	<cfset var local = structNew()>
	<cfset local.resContent =variables.content>

	<cfset local.authKey =  "a648011358421e0884fc46d3b942fda813a21fd7" />
	<cfset local.gistAPI = createObject("component","Gist").init(local.authKey)>
	<cfset local.gistTagRegex = "\[gist[^\]]*?(?:\/]|\]([\s\S]*?)\[\/gist\])">
	<cfset local.gistAttrRegex = "(\burl\b|\bpublic\b|\bfilename\b)\s*=\s*['""]([^'""]+)['""]">
	<cfset local.gistTags = rematch(local.gistTagRegex,local.resContent)>

	<!--- Loop through all gist tags --->
	<cfloop array="#local.gistTags#" index="local.gistTag">
		<cfset local.attributes=rematch(local.gistAttrRegex,htmldecode(local.gistTag))>
		<cfset local.args = structNew()>
		<cfset local.originalcontent=rereplacenocase(local.gistTag,local.gistTagRegex,"\1")>
		<cfset local.args.content = reReplaceNoCase(local.originalcontent, "<[\/]*[\S\s]+?[^>]*>",'',"All")>	<!--- Remove html tags --->
		<cfset local.args.content = htmldecode(local.args.content)>
		<cfset local.args.content = reReplaceNoCase(local.args.content,"(\n)[\t]+","\1","All")>

		<!--- Handle Attributes --->
		<cfloop array="#local.attributes#" index="local.attribute">
			<cfset local.attrname = rereplace(local.attribute,local.gistAttrRegex,"\1")>
			<cfset local.attrvalue = rereplace(local.attribute,local.gistAttrRegex,"\2")>
			<cfswitch expression="#local.attrname#">
				<cfcase value="url">
					<cfset local.args.id = listlast(local.attrvalue,"/")>
					<cfset local.args.userid = listfirst(local.attrvalue,"/")>
				</cfcase>
				<cfcase value="public">
					<cfset local.args.ispublic = listlast(local.attrvalue,"/")>
				</cfcase>
				<cfcase value="filename">
					<cfset local.args.filename = listlast(local.attrvalue,"/")>
				</cfcase>
			</cfswitch>
		</cfloop>
		<cftry>
		<cfif structKeyExists(local.args,"id")>
			<cfif len(trim(local.args.content))>	<!--- Do not update if there is not content inside --->
				<cfset local.currentGist = local.gistAPI.getGist(id=local.args.id)>
				<cfset local.args.newfilename = local.args.filename>
				<cfset local.args.filename = structkeyarray(local.currentGist.files)[1]>
				<cfset local.res =  local.gistAPI.updateGist(argumentcollection=local.args)>
				<cfset local.args.id = local.res.id>
				<cfset local.args.userid = local.res.owner.login>
				<cfset local.gistContent = htmlencode(createGISTTag(local.args)) & local.originalcontent & "[/gist]" >
				<cfset local.resContent = replacenocase(local.resContent,local.gistTag,local.gistContent)>
			</cfif>
		<cfelse>
			<cfset local.res =  local.gistAPI.createGist(argumentcollection=local.args)>
			<cfset local.args.ispublic = iif(local.res.public,de("true"),de("false"))>
			<cfset local.args.id = local.res.id>
			<cfset local.args.userid = local.res.owner.login>
			<cfset local.args.filename = structkeyarray(local.res.files)[1]>
			<cfset local.gistContent = htmlencode(createGISTTag(local.args)) & local.originalcontent & "[/gist]" >
			<cfset local.resContent = replacenocase(local.resContent,local.gistTag,local.gistContent)>
		</cfif>
		<cfcatch>
			<cfrethrow>
			<!--- Do nothing if error occurred at gist call --->
		</cfcatch>
		</cftry>
	</cfloop>

	<cfset content = local.resContent>
</cffunction>

<cffunction name="onRenderEnd" access="public" returntype="void" output="false">
	<cfargument name="event" />
	<cfset var local = {} />
	<cfset local.resContent = content />
	<cfset local.gistTagRegex = "\[gist[^\]]*?(?:\/]|\]([\s\S]*?)\[\/gist\])">
	<cfset local.gistAttrRegex = "(\burl\b)\s*=\s*['""]([^'""]+)['""]">
	<cfset local.gistTags = rematch(local.gistTagRegex,local.resContent)>

	<cfset local.gisturl = "">
	<CFDUMP VAR="#local.gistTags#">
	<cfif arrayLen(local.gistTags)>
		<cfloop array="#local.gistTags#" index="local.code">
			<cfset local.attr = rematchnocase(local.gistAttrRegex,htmldecode(local.code))>
			<cfdump var="#local.attr#">
			<cfif arrayLen(local.attr)>
				<cfset local.gitURL = rereplacenocase(local.attr[1],local.gistAttrRegex,"\2")>
				<cfset local.gistURL = '<script src="https://gist.github.com/#local.gitURL#.js"></script>'>
				<cfset local.resContent = replacenocase(local.resContent,local.code,local.gisturl)>
			</cfif>
		</cfloop>
	</cfif>
	<cfoutput>#local.resContent#</cfoutput><cfabort>
</cffunction>

<cffunction name="createGISTTag" access="private" returntype="string">
	<cfargument name="params" type="struct" required="true">
	<cfset var gistTag = "[gist " >
	<cfset var attr= "">
	<cfloop array="#structKeyArray(arguments.params)#" index="attr">
		<cfswitch expression="#attr#">
		<cfcase value="id">
			<cfset gistTag = gistTag & " url=""#arguments.params['userid']#/#arguments.params['id']#""">
		</cfcase>
		<cfcase value="ispublic">
			<cfset gistTag = gistTag & " public=""#arguments.params[attr]#""">
		</cfcase>
		<cfcase value="filename">
			<cfset gistTag = gistTag & " filename=""#arguments.params[attr]#""">
		</cfcase>
		</cfswitch>
	</cfloop>
	<cfset gistTag = gistTag & "]">
	<cfreturn gistTag>
</cffunction>

<cffunction name="htmldecode" access="private" returntype="string">
	<cfargument name="encodedStr" type="string" required="true">
	<cfreturn replacelist(arguments.encodedStr,"&lt;,&gt;,&quot;,&nbsp;","<,>,"", ")>
</cffunction>
<cffunction name="htmlencode" access="private" returntype="string">
	<cfargument name="htmlstring" type="string" required="true">
	<cfset arguments.htmlstring = replacelist(arguments.htmlstring,"<,>,"", ","&lt;,&gt;,&quot;,&nbsp;")>
	<cfreturn arguments.htmlstring>
</cffunction>
