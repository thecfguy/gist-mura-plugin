/*
Gist coldfusion wrapper allow various operation on your gist.
Following operation currently supported.
	*create
	*edit
	*delete
	*star
	*unstar
	*togglestar
	*get All Public Gist
	*get all starred gist
	*get users gists 
	
Author: Pritesh Patel
Website: www.thecfguy.com
	
	The Gist Plugin is licensed under the GPL 2.0 license.
	Copyright (C) 2013 Pritesh Patel http://www.thecfguy.com
*/
component {
	variables.authkey = "";
	variables.apiurl = "https://api.github.com";

	public function init(authkey){
		setAuthKey(arguments.authkey);
		return this;
	}

	public function getAuthKey(){
		return variables.authkey;
	}
	public function setAuthKey(value){
		variables.authkey = value;	
	}

	//API 
	public function getAllPublicGist(){
		return getGists(onlyPublic=true);
	}

	public function getMyStarredGist(){
		if(len(variables.authkey))
			return getGists(onlyStarred=true);
		else
			throw (message="Authorisation key required for starred gists", errorcode=9900);
	}	

	public function getUserGists(required string user){
		return getGists(user=arguments.user);
	}


	public function getGist(string id){
		return getGists(id=arguments.id);
	}

	private function getGists(string user,boolean onlyStarred,boolean onlyPublic, string id){
		var api = "/gists";
		if(structKeyExists(arguments,"user") and len(arguments.user))
			api = "/users/#arguments.user#" & api;
		else if(structKeyExists(arguments,"onlyStarred") and arguments.onlyStarred)
			api = api & "/starred";
		else if(structKeyExists(arguments,"onlyPublic") and arguments.onlyPublic)
			api = api & "/public";
		else if(structKeyExists(arguments,"id") and len(arguments.id))
			api = api & "/#arguments.id#";

		var httpService = new http();
		httpService.setMethod("get");
		httpService.setCharset("utf-8");
		httpService.setURL(variables.apiurl & api);

		//Add param
		if(len(variables.authkey))
			httpService.addParam(type="header",name="Authorization",value="bearer #variables.authkey#"); 
	    
	    result = httpService.send().getPrefix();
	    if(isJSON(result.filecontent) and result.responseheader.status_code eq 200)
	    	return deserializeJSON(result.filecontent);
	    else
	    	throw (message="code=#result.responseHeader.status_code#  error occured: #result.filecontent#", errorcode=9900);
	}


	public function createGist(string description="",required string filename="muragist1.txt",string content="",boolean ispublic=true){
		var gitReq = {
			"description" = arguments.description,
			"public" = arguments.ispublic,
			"files" = {
				"#arguments.filename#" = {
					"content" = arguments.content
				}
			}
		};
		return gistAddEdit(gitStruct=gitReq);
	}

	public function updateGist(required string id,string description,string filename="muragist1.txt",string newfilename, string content="",boolean ispublic){
		var gitReq = {};
		if(structKeyExists(arguments,"description"))
			gitReq["description"] = arguments.description;

		if(structKeyExists(arguments,"ispublic"))
			gitReq["public"] = arguments.ispublic;

		if(structKeyExists(arguments,"filename") and len(arguments.filename)){
			gitReq["files"] = {
				"#filename#"={
					"content" = arguments.content
				}
			};
			if(structkeyExists(arguments,"newfilename") and len(arguments.newfilename)){
				gitReq["files"]["#filename#"]["filename"]=arguments.newfilename;
			}
		}
		return gistAddEdit(gitReq,id);
	}

	private function gistAddEdit(required struct gitStruct,string id){
		var api = "/gists";
		if(structkeyExists(arguments,"id"))
			api = api & "/#id#";

		var httpService = new http();
		httpService.setMethod("POST");
		httpService.setCharset("utf-8");
		httpService.setURL(variables.apiurl & api);

		//Add param
		if(len(variables.authkey))
			httpService.addParam(type="header",name="Authorization",value="bearer #variables.authkey#"); 

		httpService.addParam(type="body",value="#serializeJSON(arguments.gitStruct)#"); 
		result = httpService.send().getPrefix();

		if(isJSON(result.filecontent) and (result.responseHeader.status_code eq 201 or result.responseHeader.status_code eq 200))
			return deserializeJSON(result.filecontent);
		else
			throw (message="code=#result.responseHeader.status_code#  error occured: #result.filecontent#", errorcode=9900);
	}

	public function deleteGist(required string id){
		var api = "/gists/#arguments.id#";

		var httpService = new http();
		httpService.setMethod("DELETE");
		httpService.setURL(variables.apiurl & api);

		//Add param
		if(len(variables.authkey))
			httpService.addParam(type="header",name="Authorization",value="bearer #variables.authkey#"); 

		result = httpService.send().getPrefix();
		if(isJSON(result.filecontent) and result.responseheader.status_code eq 204)
			return deserializeJSON(result.filecontent);
		else
			throw (message="code=#result.responseHeader.status_code#  error occured: #result.filecontent#", errorcode=9900);
	}

	public function star(required string id){
		var result = starGistOperation(id,"star");	
		if(result.code eq "204")
			return true;
		else
			throw (message="code=#result.responseHeader.status_code#  error occured: #result.filecontent#", errorcode=9900);
	}
	public function unStar(required string id){
		var result = starGistOperation(id,"unstar");
		if(result.code eq "204")
			return true;
		else
			throw (message="code=#result.responseHeader.status_code#  error occured: #result.filecontent#", errorcode=9900);
	}
	public function toggleStar(required string id){
		var result = starGistOperation(id,"toggle");	

		if(isStarred(arguments.id))
			return unStar(id);
		else
			return star(id);
	}
	public function isStarred(required string id){
		var result = starGistOperation(id,"check");	
		if(result.code eq "204")
			return true;
		else if(result.error eq "{}")
			return false;
		else{
			if(isJSON(result.error))
				result.error = deserializeJSON(result.error);
			if(isStruct(result.error) and structKeyExists(result.error,"message"))
				throw (message=result.error.message, errorcode=9900);
			else
				throw (message="code=#result.responseHeader.status_code#  error occured: #result.filecontent#", errorcode=9900);
		}
	}

	private function starGistOperation(required string id,string operation){
		var api = "/gists/#arguments.id#/star";
		var result = "";
		var httpService = new http();
		var retObj = structNew();
		httpService.setURL(variables.apiurl & api);

		switch(arguments.operation){
			case 'star':
				httpService.setMethod("PUT");
				break;
			case 'unstar':
				httpService.setMethod("DELETE");
				break;
			case 'check':
				httpService.setMethod("GET");
				break;
		}

		//Add param
		if(len(variables.authkey))
			httpService.addParam(type="header",name="Authorization",value="bearer #variables.authkey#"); 

		result = httpService.send().getPrefix();

		if(isJSON(result.filecontent) and result.responseHeader.status_code eq 204)
			return deserializeJSON(result.filecontent);
		else{
			retObj.code = result.responseheader.status_code;
			retObj.error= result.filecontent;
			return retObj;	
		}
	}

}